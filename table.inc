<?php

// $Id: $

function chemistry_table_periodic ($period) {
  $orbital = chemistry_period_orbital_count($period) - 1;
  $table = array();
  for ($y = 1; $y <= $period; $y++) {
    $line = array();
    $x = 0;
    $electrons = array();
    for ($z = 1; $z <= chemistry_orbital_electron_count($x); $z++) {
      $electrons[$z] = 0;
    }
    $line[$x] = $electrons;
    for ($x = $orbital; $x > 0; $x--) {
      $electrons = array();
      for ($z = 1; $z <= chemistry_orbital_electron_count($x); $z++) {
        $electrons[$z] = 0;
      }
      $line[$x] = $electrons;
    }
    $table[$y] = $line;
  }
  for ($number = 1; $number <= chemistry_period_end_element($period); $number++) {
    $element = chemistry_element_last_orbital($number);
    $orbitals = &$table[chemistry_element_period($number)];
    $orbital = $element["orbital"];
    $count = $element["count"];
    if (array_key_exists($orbital, $orbitals)) {
      $orbitals[$orbital][$count] = $number;
    }
  }
  return $table;
}

function chemistry_table_short ($period) {
  $periodic_table = chemistry_table_periodic($period);
  $table = array();
  $lines = array();
  $is_line = 0;
  foreach ($periodic_table as $current_period => $orbitals) {
    $line = array();
    $i = 1;
    foreach ($orbitals as $current_orbital => $electrons) {
      if ($current_orbital <= 1) {
        foreach ($electrons as $electron => $number) {
          if ($is_line) {
            $lines[$is_line][] = $number;
            $line[$i] = -$is_line;
            $is_line = 0;
          } else {
            if ($number == 2) {
              $line[$i] = 0;
            } else {
              $line[$i] = $number;
            }
          }
          $i++;
        }
      } elseif ($current_orbital == 2) {
        foreach ($electrons as $electron => $number) {
          if ($number != 0) {
            $line[$i] = $number;
            if ($i == 10) {
              $table[$current_period.'a'] = $line;
              $current_period .= 'b';
              $line = array();
            }
            $i++;
          }
        }
      } else {
        $is_line = $electrons[1];
        if ($is_line) {
          foreach ($electrons as $electron => $number) {
            $lines[$is_line][] = $number;
          }
        }
      }
    }
    $table[$current_period] = $line;
  }
  $table[1][count($table[1])] = 2;
  return array(
    "table" => $table,
    "lines" => $lines,
  );
}

function chemistry_table_standart ($period) {
  $periodic_table = chemistry_table_periodic($period);
  $table = array();
  $lines = array();
  $is_line = 0;
  foreach ($periodic_table as $current_period => $orbitals) {
    $line = array();
    $i = 1;
    foreach ($orbitals as $current_orbital => $electrons) {
      if ($current_orbital <= 2) {
        foreach ($electrons as $electron => $number) {
          if ($is_line) {
            $lines[$is_line][] = $number;
            $line[$i] = -$is_line;
            $is_line = 0;
          } else {
            if ($number == 2) {
              $line[$i] = 0;
            } else {
              $line[$i] = $number;
            }
          }
          $i++;
        }
      } else {
        $is_line = $electrons[1];
        if ($is_line) {
          foreach ($electrons as $electron => $number) {
            $lines[$is_line][] = $number;
          }
        }
      }
    }
    $table[$current_period] = $line;
  }
  $table[1][count($table[1])] = 2;
  return array(
    "table" => $table,
    "lines" => $lines,
  );
}

function chemistry_table_long ($period) {
  $periodic_table = chemistry_table_periodic($period);
  $table = array();
  $lines = array();
  foreach ($periodic_table as $current_period => $orbitals) {
    $line = array();
    $i = 1;
    foreach ($orbitals as $current_orbital => $electrons) {
      foreach ($electrons as $electron => $number) {
        if ($number == 2) {
          $line[$i] = 0;
        } else {
          $line[$i] = $number;
        }
        $i++;
      }
    }
    $table[$current_period] = $line;
  }
  $table[1][count($table[1])] = 2;
  return array(
    "table" => $table,
    "lines" => $lines,
  );
}

function chemistry_table_call ($functions, $name, $param_arr) {
  if (array_key_exists($name, $functions)) {
    $function = $functions[$name];
    if (function_exists($function)) {
	  return call_user_func_array($function, $param_arr);
    } else {
	  return $function;
    }
  } else {
    return '';
  }
}

function chemistry_table_build ($table, $data, $functions) {
  $r = '';
  $r .= chemistry_table_call($functions, 'table_start', array('data' => $data));
  $r .= chemistry_table_call($functions, 'head_row_start', array('data' => $data));
  $r .= chemistry_table_call($functions, 'head_cell', array('data' => $data, 'col' => 0, 'num' => 0, 'span' => 1));
  $cols = $table["table"][1];  
  $col_count = count($cols);
  foreach ($cols as $col => $cell) {
    if ($col_count <= 10) {
      if ($col == 8) {
        $num = 8;
        $span = 3;
      } elseif ($col > 8) {
        $num = 8;
        $span = 0;
      } else {
        $num = $col;
        $span = 1;
      }
    } elseif ($col_count <= 18) {
      $num = $col;
      $span = 1;
    } else {
      if ($col <= 2) {
        $num = $col;
        $span = 1;
      } elseif ($col == 3) {
        $num = 0;
        $span = 14;
      } elseif ($col <= 16) {
        $num = 0;
        $span = 0;
      } else {
        $num = $col - 14;
        $span = 1;
      }
    }
    $r .= chemistry_table_call($functions, 'head_cell', array('data' => $data, 'col' => $col, 'num' => $num, 'span' => $span));
  }
  $r .= chemistry_table_call($functions, 'head_row_end', array('data' => $data));
  foreach ($table["table"] as $row => $cols) {
    $len = strlen($row)-1;
    if ($row[$len] == 'a') {
      $num = substr($row, 0, $len);
      $span = 2;
    } elseif ($row[$len] == 'b') {
      $num = substr($row, 0, $len);
      $span = 0;
    } else {
      $num = $row;
      $span = 1;
    }
    $r .= chemistry_table_call($functions, 'row_start', array('data' => $data, 'row' => $row, 'num' => $num, 'span' => $span));
    $r .= chemistry_table_call($functions, 'row_head_cell', array('data' => $data, 'row' => $row, 'num' => $num, 'span' => $span));
    foreach ($cols as $col => $cell) {
      if ($cell == 0) {
        $r .= chemistry_table_call($functions, 'null_cell', array('data' => $data, 'number' => $cell));
      } else {
        $r .= chemistry_table_call($functions, 'element_cell', array('data' => $data, 'number' => $cell));
      }
    }
    $r .= chemistry_table_call($functions, 'row_end', array('data' => $data, 'row' => $row, 'num' => $num, 'span' => $span));
  }
  $r .= chemistry_table_call($functions, 'table_end', array('data' => $data));
  if (count($table["lines"]) > 0) {
    $r .= chemistry_table_call($functions, 'table_start', array('data' => $data));
    foreach ($table["lines"] as $number => $line) {
      $r .= chemistry_table_call($functions, 'line_start', array('data' => $data, 'number' => $number));
      $r .= chemistry_table_call($functions, 'line_head', array('data' => $data, 'number' => $number));
      foreach ($line as $cell) {
        $r .= chemistry_table_call($functions, 'element_cell', array('data' => $data, 'number' => $cell));
      }
      $r .= chemistry_table_call($functions, 'line_end', array('data' => $data, 'number' => $number));
    }
    $r .= chemistry_table_call($functions, 'table_end', array('data' => $data));  
  }
  return $r;
}

