<?php

// $Id: $

function chemistry_element_period ($number) {
  $period = 1;
  while ($number >= chemistry_period_start_element($period + 1)) {
    $period++;
  }
  return (int)$period;
}

function chemistry_element_top_orbitals ($number) {
  $r = array();
  $period = chemistry_element_period($number);
  $number = $number - chemistry_period_start_element($period) + 1;
  $orbital = 0;
  $level = $period;
  $r[] = chemistry_orbital_array($orbital, $level, &$number);
  $level = $period - chemistry_period_orbital_count($period) + 2;
  for ($orbital = chemistry_period_orbital_count($period) - 1; $orbital > 0; $orbital--) {
    $r[] = chemistry_orbital_array($orbital, $level, &$number);
    $level++;
  }
  return $r;
}

function chemistry_element_short_top_orbitals ($number) {
  $r = array();
  $period = chemistry_element_period($number);
  $number = $number - chemistry_period_start_element($period) + 1;
  $orbital = 0;
  $level = $period;
  $r[] = chemistry_orbital_array($orbital, $level, &$number);
  $level = $period - chemistry_period_orbital_count($period) + 2;
  for ($orbital = chemistry_period_orbital_count($period) - 1; $orbital > 0; $orbital--) {
    if ($number > 0) {
      $r[] = chemistry_orbital_array($orbital, $level, &$number);
    }
    $level++;
  }
  return $r;
}

function chemistry_element_last_orbital ($number) {
  return array_pop(chemistry_element_short_top_orbitals($number));
}

function chemistry_element_full_orbitals ($number) {
  return array_merge(chemistry_period_full_orbitals(chemistry_element_period($number) - 1), chemistry_element_top_orbitals($number));
}

function chemistry_element_short_full_orbitals ($number) {
  return array_merge(chemistry_period_full_orbitals(chemistry_element_period($number) - 1), chemistry_element_short_top_orbitals($number));
}

function chemistry_element_orbital_table ($number) {
  $period = chemistry_element_period($number);
  $r = chemistry_period_orbital_table($period);
  $orbitals = chemistry_element_full_orbitals($number);
  foreach ($orbitals as $orbital) {
    $level = $orbital["level"];
    $letter = $orbital["orbital_letter"];
    $count = $orbital["count"];
    $r[$level][$letter] = $count;
  }
  return $r;
}

function chemistry_element_is_noble_gas ($number) {
  if ($number == 2) {
    return true;
  } else {
    $last_orbital = chemistry_element_last_orbital($number);
    return (($last_orbital["orbital"] == 1) && ($last_orbital["count"] == 6));
  }  
}

function chemistry_element_is_halogen ($number) {
  $last_orbital = chemistry_element_last_orbital($number);
  return (($last_orbital["orbital"] == 1) && ($last_orbital["count"] == 5));
}

function chemistry_element_is_metalloid ($number) {
  if ($number == 5) {
    return true;
  } elseif ($number == 14) {
    return true;
  } elseif ($number == 32) {
    return true;
  } elseif ($number == 33) {
    return true;
  } elseif ($number == 51) {
    return true;
  } elseif ($number == 52) {
    return true;
  } elseif ($number == 84) {
    return true;
  } else {
    return false;
  }
}

function chemistry_element_is_nonmetal ($number) {
  $last_orbital = chemistry_element_last_orbital($number);
  if ($last_orbital["level"] == 1) {
    return true;
  } else {
    return (($last_orbital["orbital"] == 1) && (($last_orbital["count"] - $last_orbital["level"]) >= -1));
  }
}

function chemistry_element_is_metal ($number) {
  $last_orbital = chemistry_element_last_orbital($number);
  return (($last_orbital["orbital"] == 1) && (($last_orbital["count"] - $last_orbital["level"]) < -1));
}

function chemistry_element_is_alkali_metal ($number) {
  $last_orbital = chemistry_element_last_orbital($number);
  if ($last_orbital["level"] == 1) {
    return false;
  } else {
    return (($last_orbital["orbital"] == 0) && ($last_orbital["count"] == 1));
  }
}

function chemistry_element_is_alkaline_earth_metal ($number) {
  $last_orbital = chemistry_element_last_orbital($number);
  if ($last_orbital["level"] == 1) {
    return false;
  } else {
    return (($last_orbital["orbital"] == 0) && ($last_orbital["count"] == 2));
  }
}

function chemistry_element_is_transition_metal ($number) {
  $last_orbital = chemistry_element_last_orbital($number);
  return ($last_orbital["orbital"] >= 2);
}

function chemistry_element_is_lanthanide ($number) {
  $last_orbital = chemistry_element_last_orbital($number);
  return (($last_orbital["level"] == 4) && ($last_orbital["orbital"] == 3));
}

function chemistry_element_is_lanthanide_ex ($number) {
  $last_orbital = chemistry_element_last_orbital($number);
  return ((($last_orbital["level"] == 4) && ($last_orbital["orbital"] == 3)) || ($number == 71));
}

function chemistry_element_is_actinide ($number) {
  $last_orbital = chemistry_element_last_orbital($number);
  return (($last_orbital["level"] == 5) && ($last_orbital["orbital"] == 3));
}

function chemistry_element_is_actinide_ex ($number) {
  $last_orbital = chemistry_element_last_orbital($number);
  return ((($last_orbital["level"] == 5) && ($last_orbital["orbital"] == 3)) || ($number == 103));
}

if (!function_exists('variable_get')) {
  function variable_get ($name, $default) {
    return $default;
  }
}

function chemistry_element_color ($number) {
  if (chemistry_element_is_noble_gas($number)) {
    return variable_get('chemistry_noble_gas_color', "#C0FFFF");
  } elseif (chemistry_element_is_halogen($number)) {
    return variable_get('chemistry_halogen_color', "#FFFF99");
  } elseif (chemistry_element_is_metalloid($number)) {
    return variable_get('chemistry_metalloid_color', "#CCCC99");
  } elseif (chemistry_element_is_metal($number)) {
    return variable_get('chemistry_metal_color', "#CCCCCC");
  } elseif (chemistry_element_is_nonmetal($number)) {
    return variable_get('chemistry_nonmetal_color', "#A0FFA0");
  } elseif (chemistry_element_is_alkali_metal($number)) {
    return variable_get('chemistry_alkali_metal_color', "#FF6666");
  } elseif (chemistry_element_is_alkaline_earth_metal($number)) {
    return variable_get('chemistry_alkaline_earth_metal_color', "#FFDEDE");
  } elseif (chemistry_element_is_lanthanide_ex($number)) {
    return variable_get('chemistry_lanthanide_color', "#FFBFFF");
  } elseif (chemistry_element_is_actinide_ex($number)) {
    return variable_get('chemistry_actinide_color', "#EF99CC");
  } elseif (chemistry_element_is_transition_metal($number)) {
    return variable_get('chemistry_transition_metal_color', "#FFC0C0");
  } else {
    return variable_get('chemistry_default_element_color', "#FFFFFF");
  }
}

