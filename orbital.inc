<?php

// $Id: $

function chemistry_orbital_letter ($orbital) {
  if ($orbital == 0)  {
    return "s";
  } elseif ($orbital == 1) {
    return "p";
  } elseif ($orbital == 2) {
    return "d";
  } else {
    return chr($orbital - 3 + 102);
  }
}
  
function chemistry_orbital_number ($letter) {
  if ($letter == "s")  {
    return 0;
  } elseif ($letter == "p") {
    return 1;
  } elseif ($letter == "d") {
    return 2;
  } else {
    return ord($letter) + 3 - 102;
  }
}
  
function chemistry_orbital_electron_count ($orbital) {
  return (int)(2 * (2 * $orbital + 1));
}

function chemistry_orbital_length ($orbital) {
  return (int)(2 * pow($orbital + 1, 2));
}

function chemistry_orbital_array ($orbital, $level, &$number) {
  $electron_count = chemistry_orbital_electron_count($orbital);
  $r = array(
    "level" => $level,
    "level_letter" => chemistry_level_letter($level),
    "orbital" => $orbital,
    "orbital_letter" => chemistry_orbital_letter($orbital),
    "count" => $number > 0 ? ($number < $electron_count ? $number : $electron_count) : 0,
  );
  $number = $number - chemistry_orbital_electron_count($orbital);
  return $r;
}

