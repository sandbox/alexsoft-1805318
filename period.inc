<?php

// $Id: $

function chemistry_period_orbital_count ($period) {
  return (int)(floor($period / 2) + 1);
}

function chemistry_period_element_count ($period) {
  return (int)(2 * pow(floor($period / 2) + 1, 2));
}

function chemistry_period_start_element ($period) {
  $r = 1;
  for ($i = 1; $i < $period; $i++) {
    $r += chemistry_period_element_count($i);
  }
  return (int)$r;
}

function chemistry_period_end_element ($period) {
  return (int)(chemistry_period_start_element($period) + chemistry_period_element_count($period) - 1);
}

function chemistry_period_top_orbitals ($period) {
  return chemistry_element_top_orbitals(chemistry_period_end_element($period));
}

function chemistry_period_full_orbitals ($period) {
  if ($period == 0) {
    return array();
  } else {
    return array_merge(chemistry_period_full_orbitals($period - 1), chemistry_period_top_orbitals($period));
  }
}

function chemistry_period_orbital_table ($period) {
  $r = array();
  for ($level = 1; $level <= $period; $level++) {
    for ($orbital = 0; $orbital < $level; $orbital++) {
      $letter = chemistry_orbital_letter($orbital);
      $r[$level][$letter] = "null";
    }
  }
  $orbitals = chemistry_period_full_orbitals($period - 1);
  foreach ($orbitals as $orbital) {
    $level = $orbital["level"];
    $letter = $orbital["orbital_letter"];
    $r[$level][$letter] = "full";
  }
  $orbitals = chemistry_period_top_orbitals($period);
  foreach ($orbitals as $orbital) {
    $level = $orbital["level"];
    $letter = $orbital["orbital_letter"];
    $r[$level][$letter] = "curr";
  }
  return $r;
}

